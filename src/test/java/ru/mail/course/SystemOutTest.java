package ru.mail.course;

import com.google.gson.Gson;
import lombok.NoArgsConstructor;
import net.lamberto.junit.GuiceJUnitRunner;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import ru.mail.course.model.Author;
import ru.mail.course.model.Book;

import javax.inject.Inject;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static ru.mail.course.TestModule.books;
import static ru.mail.course.TestModule.getBooksCount;

@NoArgsConstructor
@RunWith(GuiceJUnitRunner.class)
@GuiceJUnitRunner.GuiceModules(TestModule.class)
public final class SystemOutTest {
    @NotNull
    private final ByteArrayOutputStream out = new ByteArrayOutputStream();
    @NotNull
    private final PrintStream systemOut = System.out;

    @SuppressWarnings("NullableProblems")
    @NotNull
    @Inject
    private LibraryFactory libraryFactory;

    @Before
    public void setUp() {
        System.setOut(new PrintStream(out));
    }

    @Test
    public void when_PrintBooks_Expect_OutBooksJsonInConsole() {
        BooksFactory booksFactory = Mockito.mock(BooksFactory.class);
        List<Book> initialBooks = books();
        Mockito.when(booksFactory.books())
                .thenReturn(initialBooks);

        Library library = libraryFactory.library(getBooksCount());
        library.printAllBooks();

        String actual = out.toString();
        Gson gson = new Gson();
        String expected = initialBooks.stream()
                .map(gson::toJson)
                .collect(Collectors.joining(System.lineSeparator(), "", System.lineSeparator()));

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void when_GetBook_Expect_OutCellIndexAndBookJson() {
        BooksFactory booksFactory = Mockito.mock(BooksFactory.class);
        Book expectedBook = new Book(new Author("a"), "b");
        Mockito.when(booksFactory.books())
                .thenReturn(Collections.singletonList(expectedBook));

        Library library = new Library(getBooksCount(), booksFactory);
        final int index = 0;
        library.getBook(index);

        String actual = out.toString();
        String expected = index + ": " + expectedBook + System.lineSeparator();
        Assert.assertEquals(expected, actual);
    }

    @After
    public void restore() {
        System.setOut(systemOut);
    }
}
