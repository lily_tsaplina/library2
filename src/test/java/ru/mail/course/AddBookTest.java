package ru.mail.course;

import com.google.inject.Guice;
import com.google.inject.Injector;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.mail.course.model.Author;
import ru.mail.course.model.Book;

import javax.inject.Inject;

import static ru.mail.course.TestModule.getBooksCount;

@NoArgsConstructor
public final class AddBookTest {
    @SuppressWarnings("NullableProblems")
    @NotNull
    @Inject
    private LibraryFactory libraryFactory;

    @Before
    public void setUpTest() {
        final Injector injector =
                Guice.createInjector(new TestModule());
        injector.injectMembers(this);
    }

    @Test
    public void when_AddBook_Expect_ItIsAddedToTheFirstEmptyCell() {
        int firstEmptyCell = getBooksCount();
        Library library = libraryFactory.library(firstEmptyCell + 2);
        Book expectedBook = new Book(new Author("a"), "b");

        library.addBook(expectedBook);
        Book actualBook = library.getBook(firstEmptyCell);

        Assert.assertEquals(expectedBook, actualBook);
    }

    @Test(expected = IllegalStateException.class)
    public void when_NoEmptyCells_Expect_Exception() {
        Library library = libraryFactory.library(getBooksCount());
        library.addBook(new Book());
    }
}
