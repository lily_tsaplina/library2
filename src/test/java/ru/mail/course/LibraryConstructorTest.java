package ru.mail.course;

import lombok.NoArgsConstructor;
import net.lamberto.junit.GuiceJUnitRunner;
import org.hamcrest.Matchers;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import ru.mail.course.model.Book;

import javax.inject.Inject;
import java.util.List;

import static org.junit.Assert.fail;
import static ru.mail.course.TestModule.books;

@NoArgsConstructor
@RunWith(GuiceJUnitRunner.class)
@GuiceJUnitRunner.GuiceModules(TestModule.class)
public final class LibraryConstructorTest {
    @SuppressWarnings("NullableProblems")
    @NotNull
    @Inject
    private LibraryFactory libraryFactory;

    @NotNull
    @Rule
    public ExpectedException exceptionGrabber = ExpectedException.none();

    @Test(expected = IllegalStateException.class)
    public void when_CapacityIsLessThanBooksSizeFromFactory_Expect_Exception() {
        libraryFactory.library(TestModule.getBooksCount() - 1);
    }

    @Test
    public void when_ConstructLibrary_Expect_BooksThereInTheSameOrder() {
        BooksFactory booksFactory = Mockito.mock(BooksFactory.class);
        List<Book> initialBooks = books();
        Mockito.when(booksFactory.books())
                .thenReturn(initialBooks);

        final int capacity = initialBooks.size() + 2;
        Library library = libraryFactory.library(capacity);
        Book[] expectedBooks = new Book[initialBooks.size()];
        initialBooks.toArray(expectedBooks);

        for (int i = 0; i < expectedBooks.length; i++) {
            Assert.assertEquals(library.getBook(i), expectedBooks[i]);
        }

        // check that rest cells are empty
        for (int i = expectedBooks.length; i < capacity; i++) {
            try {
                library.getBook(i);
                fail();
            } catch (IllegalArgumentException ignored) {
            }
        }
    }
}
