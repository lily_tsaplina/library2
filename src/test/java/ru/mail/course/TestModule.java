package ru.mail.course;

import com.google.inject.AbstractModule;
import org.jetbrains.annotations.NotNull;
import ru.mail.course.model.Author;
import ru.mail.course.model.Book;

import java.util.Arrays;
import java.util.List;

public final class TestModule extends AbstractModule {
    private static final int BOOKS_COUNT = 2;

    @Override
    protected void configure() {
        bind(BooksFactory.class).toInstance(TestModule::books);
        bind(LibraryFactory.class).to(LibraryFactoryImpl.class);
    }

    static int getBooksCount() {
        return BOOKS_COUNT;
    }

    @NotNull
    static List<Book> books() {
        Book[] books = new Book[BOOKS_COUNT];
        for (int i = 0; i < BOOKS_COUNT; i++) {
            books[i] = new Book(new Author("Author" + i), "Name" + i);
        }
        return Arrays.asList(books);
    }
}
