package ru.mail.course;

import lombok.NoArgsConstructor;
import net.lamberto.junit.GuiceJUnitRunner;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import ru.mail.course.model.Author;
import ru.mail.course.model.Book;

import javax.inject.Inject;
import java.util.Collections;

@NoArgsConstructor
@RunWith(GuiceJUnitRunner.class)
@GuiceJUnitRunner.GuiceModules(TestModule.class)
public final class GetBookTest {
    @SuppressWarnings("NullableProblems")
    @NotNull
    @Inject
    private LibraryFactory libraryFactory;

    @Test (expected = IllegalArgumentException.class)
    public void when_GetBookFromEmptyCell_Expect_Exception() {
        Library library = libraryFactory.library(TestModule.getBooksCount() + 5);
        library.getBook(TestModule.getBooksCount());
    }


    @Test
    public void when_GetBook_Expect_ReturnsBookEqualsBookInTheCell() {
        Book expectedBook = new Book(new Author("Author0"), "Name0");
        BooksFactory booksFactory = Mockito.mock(BooksFactory.class);

        Mockito.when(booksFactory.books())
                .thenReturn(Collections.singletonList(expectedBook));

        Library library = new Library(5, booksFactory);
        Assert.assertEquals(expectedBook, library.getBook(0));
    }
}