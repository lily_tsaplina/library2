package ru.mail.course.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

/**
 * Author model
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public final class Author {
    @NotNull
    private String name;
}
