package ru.mail.course.model;

import lombok.*;
import org.jetbrains.annotations.NotNull;

/**
 * Book model.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public final class Book {
    @NotNull
    private Author author;

    @NotNull
    private String name;
}
