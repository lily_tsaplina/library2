package ru.mail.course;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.jetbrains.annotations.NotNull;

/**
 * Class with application entry point.
 */
public final class Application {
    /**
     * Application entry point.
     *
     * @param args program arguments
     */
    public static void main(@NotNull String[] args) {
        if (args.length == 2) {
            String fileName = args[0];
            int capacity = Integer.parseInt(args[1]);

            Injector injector = Guice.createInjector(new LibraryGuiceModule(fileName));
            LibraryFactory libraryFactory = injector.getInstance(LibraryFactory.class);
            Library library = libraryFactory.library(capacity);
            library.printAllBooks();
        } else {
            throw new IllegalArgumentException("Incorrect program args! Should be string (json file name with books) and positive integer (library capacity).");
        }
    }
}
