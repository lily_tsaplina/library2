package ru.mail.course;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.jetbrains.annotations.NotNull;
import ru.mail.course.model.Book;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Factory to create books from json file.
 */
public final class FileBooksFactory implements BooksFactory {
    @NotNull
    private static final Type booksListType = new TypeToken<ArrayList<Book>>() {}.getType();
    @NotNull
    private final String fileName;

    public FileBooksFactory(@NotNull String fileName) {
        this.fileName = fileName;
    }

    @NotNull
    @Override
    public Collection<Book> books() {
        try {
            return new Gson().fromJson(new BufferedReader(new FileReader(fileName)), booksListType);
        } catch (FileNotFoundException e) {
            throw new IllegalStateException(e);
        }
    }
}
