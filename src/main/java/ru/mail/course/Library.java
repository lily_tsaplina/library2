package ru.mail.course;

import com.google.gson.Gson;
import org.jetbrains.annotations.NotNull;
import ru.mail.course.model.Book;

import javax.inject.Inject;
import java.util.Collection;
import java.util.Objects;

/**
 * Library to manage books (books controller).
 */
public final class Library {
    @NotNull
    private Book[] cells;

    private int capacity;

    /**
     * Constructor.
     *
     * @param capacity library capacity
     * @param booksFactory factory to create books and put into library
     */
    @Inject
    public Library(int capacity, @NotNull BooksFactory booksFactory) {
        Collection<Book> books = booksFactory.books();
        if (books.size() > capacity) {
            throw new IllegalStateException("Not enough cells in library for books from factory!");
        } else {
            this.capacity = capacity;
            this.cells = new Book[capacity];
            books.toArray(this.cells);
        }
    }

    /**
     * Get book from the cell.
     *
     * @param i index of the cell
     * @return book if the cell exists and is not empty
     */
    @NotNull
    public Book getBook(int i) {
        if (i < 0 || i > capacity - 1) {
            throw new IllegalArgumentException("No cell with such a number");
        }
        Book book = cells[i];
        if (book == null) {
            throw new IllegalArgumentException("The cell is empty!");
        }
        cells[i] = null;
        System.out.println(i + ": " + book);
        return book;
    }

    /**
     * Add book to the first empty cell if it exists else exception to be thrown.
     *
     * @param book book to add
     */
    public void addBook(@NotNull Book book) {
        for (int i = 0; i < cells.length; i++) {
            if (Objects.isNull(cells[i])) {
                cells[i] = book;
                return;
            }
        }
        throw new IllegalStateException("No free cells for books in library!");
    }

    /**
     * Print books in json format to console.
     */
    public void printAllBooks() {
        for (Book book : cells) {
            System.out.println(new Gson().toJson(book));
        }
    }
}
