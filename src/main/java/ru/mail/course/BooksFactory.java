package ru.mail.course;

import org.jetbrains.annotations.NotNull;
import ru.mail.course.model.Book;

import java.util.Collection;

/**
 * Factory to create {@link Book} objects.
 */
public interface BooksFactory {
    @NotNull
    Collection<Book> books();
}
