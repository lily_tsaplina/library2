package ru.mail.course;

import com.google.inject.AbstractModule;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;

/**
 * Module to configure Guice bindings.
 */
@RequiredArgsConstructor
public final class LibraryGuiceModule extends AbstractModule {
    @NotNull
    private final String fileName;

    @Override
    protected void configure() {
        bind(LibraryFactory.class).to(LibraryFactoryImpl.class);
        bind(BooksFactory.class).toInstance(new FileBooksFactory(fileName));
    }
}
