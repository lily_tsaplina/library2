package ru.mail.course;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

/**
 * Factory to get {@link Library}.
 */
public final class LibraryFactoryImpl implements LibraryFactory {
    @NotNull
    private BooksFactory booksFactory;

    @Inject
    public LibraryFactoryImpl(@NotNull BooksFactory booksFactory) {
        this.booksFactory = booksFactory;
    }

    @Override
    @NotNull
    public Library library(int capacity) {
        return new Library(capacity, booksFactory);
    }
}
