package ru.mail.course;

import org.jetbrains.annotations.NotNull;

public interface LibraryFactory {
    @NotNull
    Library library(int capacity);
}
